﻿using UnityEngine;

public static class Noise
{
	public enum NormalizeMode
	{
		Local,
		Global
	}

	public static float[,] GenerateNoiseMap (int mapWidth, int mapHeight, int seed, float scale, int octaves, float persistance, float lacunarity, Vector2 offset, NormalizeMode normalizedMode) {
		float[,] noiseMap = new float[mapWidth, mapHeight];

		System.Random prng = new System.Random (seed);
		Vector2[] octaveOffsets = new Vector2[octaves];

		float maxPossibleHeight = 0;
		float amplitude = 1, frequency = 1; 

		for (int octave = 0; octave < octaves; octave++) {
			float offsetX = prng.Next (-100000, 100000) + offset.x;
			float offsetY = prng.Next (-100000, 100000) - offset.y;

			octaveOffsets [octave] = new Vector2 (offsetX, offsetY);

			maxPossibleHeight += amplitude;
			amplitude *= persistance;
		}

		scale = Mathf.Max (scale, 0.0001f);

		float maxLocalNoiseHeight = float.MinValue;
		float minLocalNoiseHeight = float.MaxValue;

		float halfWidth = mapWidth / 2f;
		float halfHeight = mapHeight / 2f;

		for (int y = 0; y < mapHeight; y++) {
			for (int x = 0; x < mapWidth; x++) {

				amplitude = 1;
				frequency = 1; 
				float noiseHeight = 0;

				for (int octave = 0; octave < octaves; octave++) {
					float sampleX = (x - halfWidth + octaveOffsets [octave].x) / scale * frequency;
					float sampleY = (y - halfHeight + octaveOffsets [octave].y) / scale * frequency;

					// Get perlin in range -1 to 1
					float perlinValue = Mathf.PerlinNoise (sampleX, sampleY) * 2 - 1;
					noiseHeight += perlinValue * amplitude;

					amplitude *= persistance;
					frequency *= lacunarity;
				}

				noiseMap [x, y] = noiseHeight;

				maxLocalNoiseHeight = Mathf.Max (maxLocalNoiseHeight, noiseHeight);
				minLocalNoiseHeight = Mathf.Min (minLocalNoiseHeight, noiseHeight);
			}	
		}

		// Normalize values
		for (int y = 0; y < mapHeight; y++) {
			for (int x = 0; x < mapWidth; x++) {

				if (normalizedMode == NormalizeMode.Local) {
					noiseMap [x, y] = Mathf.InverseLerp (minLocalNoiseHeight, maxLocalNoiseHeight, noiseMap [x, y]);
				} else {
					float normalizedHeight = (noiseMap [x, y] + 1) / (maxPossibleHeight);
					noiseMap [x, y] = Mathf.Clamp (normalizedHeight, 0, int.MaxValue);
				}

			}
		}

		return noiseMap;
	}
}
