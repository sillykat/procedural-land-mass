﻿using UnityEngine;
using System.Collections.Generic;

public class EndlessTerrain : MonoBehaviour
{
	const float scale = 5f;

	const float viewerMoveThresholdForChunkUpdate = 25f;
	const float sqrViewerMoveThresholdForChunkUpdate = viewerMoveThresholdForChunkUpdate * viewerMoveThresholdForChunkUpdate;

	public LODInfo[] detailLevels;
	public static float maxViewDistance;

	public Transform viewer;
	public Material terrainMaterial;

	public static Vector2 viewerPosition;
	public Vector2 viewerPositionOld;
	static MapGenerator mapGenerator;

	int chunkSize;
	int chunksVisibleInViewDist;

	Dictionary<Vector2, TerrainChunk> terrainChunkDictionary = new Dictionary<Vector2, TerrainChunk> ();
	static List<TerrainChunk> terrainChunksVisibleLastUpdate = new List<TerrainChunk> ();

	void Start () {
		mapGenerator = FindObjectOfType<MapGenerator> ();
		maxViewDistance = detailLevels [detailLevels.Length - 1].visibleDistThreshold;

		chunkSize = MapGenerator.mapChunkSize - 1;
		chunksVisibleInViewDist = Mathf.RoundToInt (maxViewDistance / chunkSize);

		UpdateVisibleChunks ();
	}

	void Update () {
		viewerPosition = new Vector2 (viewer.position.x, viewer.position.z) / scale;

		if ((viewerPositionOld - viewerPosition).sqrMagnitude > sqrViewerMoveThresholdForChunkUpdate) {
			UpdateVisibleChunks ();
		}
	}

	void UpdateVisibleChunks () {

		foreach (TerrainChunk chunk in terrainChunksVisibleLastUpdate) {
			chunk.IsVisible = false;
		}

		terrainChunksVisibleLastUpdate.Clear ();

		int currentChunkCoordX = Mathf.RoundToInt (viewerPosition.x / chunkSize);
		int currentChunkCoordY = Mathf.RoundToInt (viewerPosition.y / chunkSize);

		for (int yOffset = -chunksVisibleInViewDist; yOffset <= chunksVisibleInViewDist; yOffset++) {
			for (int xOffset = -chunksVisibleInViewDist; xOffset <= chunksVisibleInViewDist; xOffset++) {
				Vector2 viewedChunkCoord = new Vector2 (currentChunkCoordX + xOffset, currentChunkCoordY + yOffset);

				if (terrainChunkDictionary.ContainsKey (viewedChunkCoord)) {

					TerrainChunk chunk = terrainChunkDictionary [viewedChunkCoord];
					chunk.UpdateTerrainChunk ();

				} else {
					terrainChunkDictionary.Add (viewedChunkCoord, new TerrainChunk (viewedChunkCoord, chunkSize, detailLevels, transform, terrainMaterial));
				}
			}
		}
	}

	public class TerrainChunk
	{
		GameObject meshObject;
		Vector2 position;
		Bounds bounds;

		MeshRenderer meshRenderer;
		MeshFilter meshFilter;

		LODInfo[] detailLevels;
		LODMesh[] lodMeshes;

		MapData mapData;
		bool mapDataReceived;
		int previousLODIndex = -1;

		public bool IsVisible {
			get { return meshObject.activeSelf; }
			set { meshObject.SetActive (value); }
		}

		public TerrainChunk (Vector2 coord, int size, LODInfo[] detailLevels, Transform parent, Material material) {
			position = coord * size;
			bounds = new Bounds (position, Vector2.one * size);
			Vector3 posV3 = new Vector3 (position.x, 0, position.y);

			this.detailLevels = detailLevels;

			meshObject = new GameObject ("Terrain Chunk");

			meshFilter = meshObject.AddComponent<MeshFilter> ();
			meshRenderer = meshObject.AddComponent<MeshRenderer> ();
			meshRenderer.material = material;


			meshObject.transform.position = posV3 * scale;
			meshObject.transform.SetParent (parent, true);
			meshObject.transform.localScale = Vector3.one * scale;

			IsVisible = false;

			lodMeshes = new LODMesh[detailLevels.Length];

			for (int i = 0; i < detailLevels.Length; i++) {
				lodMeshes [i] = new LODMesh (detailLevels [i].lod, UpdateTerrainChunk);
			}

			mapGenerator.RequestMapData (position, OnMapDataReceived);
		}

		void OnMapDataReceived (MapData mapData) {
			this.mapData = mapData;
			mapDataReceived = true;

			Texture2D texture = TextureGenerator.TextureFromColourMap (mapData.colourMap, MapGenerator.mapChunkSize, MapGenerator.mapChunkSize);
			meshRenderer.material.mainTexture = texture;

			UpdateTerrainChunk ();
		}

		void OnMeshDataReceived (MeshData meshData) {
			meshFilter.mesh = meshData.CreateMesh ();
		}

		public void UpdateTerrainChunk () {

			if (mapDataReceived) {
				float viewerDistFromNearestEdge = Mathf.Sqrt (bounds.SqrDistance (viewerPosition));
				bool isVisible = viewerDistFromNearestEdge <= maxViewDistance;

				if (isVisible) {
					int lodIndex = 0;

					for (int i = 0; i < detailLevels.Length - 1; i++) {
						if (viewerDistFromNearestEdge > detailLevels [i].visibleDistThreshold) {
							lodIndex = i + 1;
						} else {
							break;
						}
					}

					if (lodIndex != previousLODIndex) {
						LODMesh lodMesh = lodMeshes [lodIndex];

						if (lodMesh.hasMesh) {
							previousLODIndex = lodIndex;
							meshFilter.mesh = lodMesh.mesh;
						} else if (!lodMesh.hasRequestedMesh) {
							lodMesh.RequestMesh (mapData);
						}
					}

					terrainChunksVisibleLastUpdate.Add (this);	
				}

				IsVisible = isVisible;
			}
		}
	}

	class LODMesh
	{

		public Mesh mesh;
		public bool hasRequestedMesh;
		public bool hasMesh;
		int lod;
		System.Action updateCallback;

		public LODMesh (int lod, System.Action updateCallback) {
			this.lod = lod;
			this.updateCallback = updateCallback;
		}

		void OnMeshDataReceived (MeshData meshData) {
			mesh = meshData.CreateMesh ();
			hasMesh = true;

			updateCallback ();
		}

		public void RequestMesh (MapData mapData) {
			hasRequestedMesh = true;
			mapGenerator.RequestMeshData (mapData, lod, OnMeshDataReceived);
		}
	}

	[System.Serializable]
	public struct LODInfo
	{
		public int lod;
		public float visibleDistThreshold;
	}
}

