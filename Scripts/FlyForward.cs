﻿using UnityEngine;
using System.Collections;

public class FlyForward : MonoBehaviour
{

	public float speed = 5f;
	
	// Update is called once per frame
	void Update () {
		this.transform.position += this.transform.forward * (speed * Time.deltaTime * Time.timeScale);
	}
}
