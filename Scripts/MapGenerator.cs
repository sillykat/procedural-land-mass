﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Threading;

[System.Serializable]
public struct TerrainType
{
	public string name;
	public float height;
	public Color colour;
}

public class MapGenerator : MonoBehaviour
{

	public enum DrawMode
	{
		NoiseMap,
		ColourMap,
		Mesh,
		FalloffMap
	}

	public DrawMode drawMode = DrawMode.NoiseMap;
	public Noise.NormalizeMode normalizeMode;

	public bool autoUpdate = true;

	public const int mapChunkSize = 241;
	[Range (0, 6)]
	public int editorPreviewLOD;

	public int seed;
	public Vector2 offset;
	public float noiseScale;
	public bool useFalloff;

	public int octaves;

	[Range (0, 1)]
	public float persistance;
	public float lacunarity;

	public float meshHeightMultiplier;
	public AnimationCurve meshHeightCurve;

	public TerrainType[] regions;

	float[,] falloffMap;

	void Awake () {
		falloffMap = FalloffGenerator.GenerateFalloffMap (mapChunkSize);
	}


	#region Editor Methods

	void OnValidate () {
		
		if (octaves < 0)
			octaves = 0;
		
		if (lacunarity < 1)
			lacunarity = 1;
		
		falloffMap = FalloffGenerator.GenerateFalloffMap (mapChunkSize);
	}

	public void DrawMapInEditor () {

		MapData mapData = GenerateMapData (Vector2.zero);
		MapDisplay display = GetComponent<MapDisplay> ();

		if (drawMode == DrawMode.NoiseMap) {
			display.DrawTexture (TextureGenerator.TextureFromHeightMap (mapData.heightMap));
		} else if (drawMode == DrawMode.ColourMap) {
			display.DrawTexture (TextureGenerator.TextureFromColourMap (mapData.colourMap, mapChunkSize, mapChunkSize));
		} else if (drawMode == DrawMode.Mesh) {
			display.DrawMesh (MeshGenerator.GenerateTerrainMesh (mapData.heightMap, meshHeightMultiplier, meshHeightCurve, editorPreviewLOD), TextureGenerator.TextureFromColourMap (mapData.colourMap, mapChunkSize, mapChunkSize));
		} else if (drawMode == DrawMode.FalloffMap) {
			display.DrawTexture (TextureGenerator.TextureFromHeightMap (FalloffGenerator.GenerateFalloffMap (mapChunkSize)));
		}
	}

	#endregion

	#region Threading

	struct MapThreadInfo<T>
	{

		public readonly Action<T> callback;
		public readonly T parameter;

		public MapThreadInfo (Action<T> callback, T parameter) {
			this.callback = callback;
			this.parameter = parameter;
		}
	}

	Queue<MapThreadInfo<MapData>> mapDataThreadInfoQueue = new Queue<MapThreadInfo<MapData>> ();
	Queue<MapThreadInfo<MeshData>> meshDataThreadInfoQueue = new Queue<MapThreadInfo<MeshData>> ();

	public void RequestMapData (Vector2 center, Action<MapData> callback) {

		ThreadStart threadStart = delegate {
			MapDataThread (center, callback);
		};

		new Thread (threadStart).Start (); 
	}

	public void RequestMeshData (MapData mapData, int lod, Action<MeshData> callback) {
		
		ThreadStart threadStart = delegate {
			MeshDataThread (mapData, lod, callback);
		};
		
		new Thread (threadStart).Start (); 
	}

	void MapDataThread (Vector2 center, Action<MapData> callback) {

		MapData mapData = GenerateMapData (center);
		lock (mapDataThreadInfoQueue) {
			mapDataThreadInfoQueue.Enqueue (new MapThreadInfo<MapData> (callback, mapData));
		}
	}

	void MeshDataThread (MapData mapData, int lod, Action<MeshData> callback) {

		MeshData meshData = MeshGenerator.GenerateTerrainMesh (mapData.heightMap, meshHeightMultiplier, meshHeightCurve, lod);
		lock (meshDataThreadInfoQueue) {
			meshDataThreadInfoQueue.Enqueue (new MapThreadInfo<MeshData> (callback, meshData));
		}
	}

	#endregion

	void Update () {

		if (mapDataThreadInfoQueue.Count > 0) {

			for (int i = 0; i < mapDataThreadInfoQueue.Count; i++) {
				MapThreadInfo<MapData> threadInfo = mapDataThreadInfoQueue.Dequeue ();
				threadInfo.callback (threadInfo.parameter);
			}
		}

		if (meshDataThreadInfoQueue.Count > 0) {

			for (int i = 0; i < meshDataThreadInfoQueue.Count; i++) {
				MapThreadInfo<MeshData> threadInfo = meshDataThreadInfoQueue.Dequeue ();
				threadInfo.callback (threadInfo.parameter);
			}
		}
	}

	MapData GenerateMapData (Vector2 center) {
		float[,] noiseMap = Noise.GenerateNoiseMap (mapChunkSize, mapChunkSize, seed, noiseScale, octaves, persistance, lacunarity, center + offset, normalizeMode);
		Color[] colourMap = new Color[mapChunkSize * mapChunkSize];

		for (int y = 0; y < mapChunkSize; y++) {
			for (int x = 0; x < mapChunkSize; x++) {

				if (useFalloff) {
					noiseMap [x, y] = Mathf.Clamp01 (noiseMap [x, y] - falloffMap [x, y]);
				}

				float currHeight = noiseMap [x, y];

				for (int i = 0; i < regions.Length; i++) {
					if (currHeight >= regions [i].height) {
						colourMap [y * mapChunkSize + x] = regions [i].colour;
					} else {
						break;
					}
				}
			}
		}

		return new MapData (noiseMap, colourMap);
	}
	
}

public struct MapData
{
	public readonly float[,] heightMap;
	public readonly Color[] colourMap;

	public MapData (float[,] heightMap, Color[] colourMap) {
		this.heightMap = heightMap;
		this.colourMap = colourMap;
	}
	
}